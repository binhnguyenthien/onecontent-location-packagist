# onecontent-location
Manage all location of website

## Publish package
**php artisan vendor:publish --tag=oc_location_configs**
**php artisan vendor:publish --tag=oc_location_plugins** 
**php artisan vendor:publish --tag=oc_location_assets**

## Import Data From Excel
**php artisan db:seed --class="OneContent\Location\Seeds\DatabaseSeeder"**
## Cơ sở dữ liệu nguồn
**https://thongtin.diagioi.gov.vn/tracuubando.aspx**

## Import Data From SQL Script file
**php artisan db:seed --class="OneContent\Location\Seeds\DatabaseSeeder"**

