@extends('oc_core::layouts.master')
@section('content')
    <section class="backend-page">
        <div class="body">
            @component('oc_core::layouts.partials.components.heading')
            @endcomponent
            @component('oc_core::layouts.partials.components.form', [
                'form_action' => empty($data) ? route('country.store') : route('country.update', [$data->id]),
                'form_method' => !empty($data) ? 'put' : NULL
            ])
                @component('oc_core::layouts.partials.components.tab_panel', [
                    'data' => $data ?? null,
                    'default_tabs' => [
                        [
                            'key' => 'general',
                            'text' => trans('oc_location::country.form.tab.general'),
                            'path' => 'oc_location::country.partials.general',
                        ]
                    ],
                    'locale_path' => 'oc_location::country.partials.translation'
                ])

                @endcomponent
            @endcomponent
        </div>
    </section>
@endsection

