
<div class="form-line">
    <label for="{{ $lang }}_name">{!! trans('oc_location::country.form.name') !!}</label>
    <input class="form-input"
           type="text"
           name="{{ $lang }}[name]"
           value="{{ old("{$lang}.name", $data->{"name:{$lang}"} ?? '') }}"
           {{ $lang == LaravelLocalization::getDefaultLocale() ? 'required' : '' }}
           id="{{ $lang }}_name"
    />
</div>
