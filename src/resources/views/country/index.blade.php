@extends('oc_core::layouts.master')
@section('content')
    <section class="backend-page">
        <div class="body">
            @component('oc_core::layouts.partials.components.heading', [
                'container_class' => 'small-margin'
            ])
                <a class="btn-ssi be" href="#modal-nhap-du-lieu" data-toggle="modal">
                    <i class="icon be-import-red"></i>
                    <span>{!! trans('oc_location::country.btn_import') !!}</span>
                </a>
                <a class="btn-ssi be" href="{{ route('country.create') }}">
                    <i class="icon be-create-red"></i>
                    <span>{!! trans('oc_location::country.btn_create') !!}</span>
                </a>
            @endcomponent
            <table class="table table-be">
                <thead>
                <tr>
                    <th>{!! trans('oc_location::country.table.no_order') !!}</th>
                    <th>{!! trans('oc_location::country.table.code') !!}</th>
                    <th>{!! trans('oc_location::country.table.name') !!}</th>
                    <th>{!! trans('oc_location::country.table.display_order') !!}</th>
                    <th>{!! trans('oc_location::country.table.status') !!}</th>
                    <th>{!! trans('oc_location::country.table.action') !!}</th>
                </tr>
                </thead>
            </table>
        </div>
    </section>
    <input type="hidden" value="{{ route('country.datatable') }}" id="link-datatable" />
    <div class="modal fade modal-edit modal-zoom" id="modal-nhap-du-lieu" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered form-style label-bold" role="document">
            <form class="modal-content form-validate" action="{!! route('country.import') !!}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h4 class="title-page no-margin">{!! trans('oc_location::country.import_modal.title') !!}</h4>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-block form-import-data">
                        <div class="row gutter-20">
                            <div class="col-sm-6">
                                <a class="big-btn" href="https://thongtin.diagioi.gov.vn/tracuubando.aspx" download target="_blank">
                                    <div class="wrap"><i class="icon be-download-modal"></i><span>{!! trans('oc_location::country.import_modal.download_template') !!}</span></div>
                                </a>
                            </div>
                            <div class="col-sm-6">
                                <label class="big-btn">
                                    <div class="wrap"><i class="icon be-upload-modal"></i><span>{!! trans('oc_location::country.import_modal.upload_file') !!}</span></div>
                                    <input required data-msg="{!! trans('oc_location::country.errors.empty_file') !!}" data-accept-msg="{!! trans('oc_location::country.errors.file_invalid') !!}" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel," id="file-import-location" type="file" name="import_file">
                                </label>
                            </div>
                        </div>
                    </div>
                    <div id="file-import-location-name" class="text-center text-info"></div>
                </div>
                <div class="modal-footer">
                    <button id="btn-submit-import-location" class="btn-ssi be red" disabled type="submit">{!! trans('oc_location::country.btn_done') !!}</button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
    @include('oc_core::layouts.partials.modal_delete_record')
    <script>
        jQuery(function($){
            var linkDataTable = $('#link-datatable').val();
            $('.table-be').dataTable({
                ajax: {
                    url: linkDataTable
                },
                order: [
                    [3, 'asc']
                ],
                dom: '<"row"<"col-md-7 offset-md-5"f>><"table-responsive"rt><"row"<"col-md-5"l><"col-md-7"p>><"clear">',
                columns: [
                    datatableRenderColumnNoOrder(),
                    {name: 'code', data: 'code', className: 'text-center'},
                    {name: 'country_translations.name', data: 'name'},
                    {name: 'display_order', data: 'display_order', className: 'text-center'},
                    {name: 'active_text', data: 'active_text', className: 'text-center', searchable: false, orderable: false},
                    {name: 'action', data: 'action', className: 'text-center has-btn', searchable: false, orderable: false},
                ]
            }).on( 'draw.dt', function () {
                $(this).dataTable_init_js();
            });

            $('.table-be').on('click', '.btn-delete', function(e) {
                e.preventDefault();
                e.stopPropagation();
                var ele = e.currentTarget;
                var link = $(ele).attr('href');
                var msg = $(ele).attr('data-msg');
                if (!link || !msg) {
                    return;
                }
                $('#modal-delete-record .content').html(msg);
                $('#modal-delete-record form').prop({
                    action: link
                });
                $('#modal-delete-record').modal('show');
            });

            $('#file-import-location').on('change', function() {
                if (this.files.length <= 0) {
                    $('#file-import-location-name').empty();
                    $('#btn-submit-import-location').prop({
                        disabled: true
                    });
                    return;
                }
                let file = this.files[0];
                $('#file-import-location-name').text(file.name);
                $('#btn-submit-import-location').prop({
                    disabled: false
                });
            });
        });
    </script>
@endsection
