@extends('oc_core::layouts.master')
@section('content')
    <section class="backend-page">
        <div class="body">
            @component('oc_core::layouts.partials.components.heading')
            @endcomponent
            @component('oc_core::layouts.partials.components.form', [
                'form_action' => empty($data) ? route('ward.store') : route('ward.update', [$data->id]),
                'form_method' => !empty($data) ? 'put' : NULL
            ])
                @component('oc_core::layouts.partials.components.tab_panel', [
                    'data' => $data ?? null,
                    'default_tabs' => [
                        [
                            'key' => 'general',
                            'text' => trans('oc_location::ward.form.tab.general'),
                            'path' => 'oc_location::ward.partials.general',
                            'includes_data' => [
                                'countries' => $countries
                            ]
                        ]
                    ],
                    'locale_path' => 'oc_location::ward.partials.translation'
                ])

                @endcomponent
            @endcomponent
        </div>
    </section>
@endsection
@section('script')
    <script>
        var numberFormat = Intl.NumberFormat();

        jQuery(function($) {
            $('#city_id').select2({
                dropdownCssClass: 'select2-dropdown-be',
                ajax: {
                    url: $('#city_id').attr('data-ajax-link'),
                    data: function (params) {
                        var query = {
                            keyword: params.term,
                            country_id: $('#country_id').val()
                        }
                        return query;
                    },
                    processResults: function (response) {
                        let results = response.data.map(function(it) {
                            return {
                                id: it.id,
                                text: it.name
                            };
                        });
                        results.unshift({
                            id: '0',
                            text: '---'
                        });
                        return {
                            results: results
                        };
                    }
                }
            });

            $('#district_id').select2({
                dropdownCssClass: 'select2-dropdown-be',
                ajax: {
                    url: $('#district_id').attr('data-ajax-link'),
                    data: function (params) {
                        var query = {
                            keyword: params.term,
                            city_id: $('#city_id').val()
                        }
                        return query;
                    },
                    processResults: function (response) {
                        let results = response.data.map(function(it) {
                            return {
                                id: it.id,
                                text: it.name
                            };
                        });
                        results.unshift({
                            id: '0',
                            text: '---'
                        });
                        return {
                            results: results
                        };
                    }
                }
            });

            $('#country_id').on('change', function () {
                $('#city_id').val(null).trigger('change');
            });
            $('#city_id').on('change', function () {
                $('#district_id').val(null).trigger('change');
            });
        });
    </script>
@endsection

