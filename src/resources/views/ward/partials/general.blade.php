
<div class="row">
    <div class="col-md-3">
        <div class="form-line">
            <label for="country_id">
                {!! trans('oc_location::ward.form.country_id') !!}

                <abbr>*</abbr>
            </label>
            <select id="country_id" name="country_id" class="form-input select-ui for-be" required data-placeholder="---">
                @foreach($countries as $it)
                    <option value="{{ $it->id }}" {{ !empty($data) && $data->district->city->country_id == $it->id ? 'selected' : '' }}>
                        {{ $it->name }}
                    </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-line">
            <label for="city_id">
                {!! trans('oc_location::ward.form.city_id') !!}

                <abbr>*</abbr>
            </label>
            <select data-ajax-link={{ route('api.cities.index') }} id="city_id" name="city_id" class="form-input" required data-placeholder="---">
                @if(!empty($data))
                    <option value="{{ $data->district->city_id }}" selected }}>
                        {{ $data->district->city_name }}
                    </option>
                @endif
            </select>
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-line">
            <label for="city_id">
                {!! trans('oc_location::ward.form.district_id') !!}

                <abbr>*</abbr>
            </label>
            <select data-ajax-link={{ route('api.districts.index') }} id="district_id" name="district_id" class="form-input" required data-placeholder="---">
                @if(!empty($data))
                    <option value="{{ $data->district_id }}" selected }}>
                        {{ $data->district_name }}
                    </option>
                @endif
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-line">
            <label for="display_order">{!! trans('oc_location::ward.form.display_order') !!}</label>
            <input class="form-input"
                   type="number"
                   name="display_order"
                   value="{{ old('display_order', $data->display_order ?? '') }}"
                   id="display_order"
            />
        </div>
    </div>
</div>
<div class="row gutter-20">
    <div class="col-xl-4 col-md-4 form-line has-checkbox for-be">
        <label for="active">
            <input id="active" value="1" type="checkbox" name="active" {{ old('active', $data->active ?? 1) == 1 ? 'checked' : '' }}>
            <span>{!! trans('oc_location::ward.status_active') !!}</span>
        </label>
    </div>
</div>
