@extends('oc_core::layouts.master')
@section('content')
    <section class="backend-page">
        <div class="body">
            @component('oc_core::layouts.partials.components.heading', [
                'container_class' => 'small-margin'
            ])
                <a class="btn-ssi be" href="{{ route('district.create') }}">
                    <i class="icon be-create-red"></i>
                    <span>{!! trans('oc_location::district.btn_create') !!}</span>
                </a>
            @endcomponent
            <table class="table table-be">
                <thead>
                <tr>
                    <th>{!! trans('oc_location::district.table.no_order') !!}</th>
                    <th>{!! trans('oc_location::district.table.name') !!}</th>
                    <th>{!! trans('oc_location::district.table.city_id') !!}</th>
                    <th>{!! trans('oc_location::district.table.display_order') !!}</th>
                    <th>{!! trans('oc_location::district.table.status') !!}</th>
                    <th>{!! trans('oc_location::district.table.action') !!}</th>
                </tr>
                </thead>

            </table>
        </div>
    </section>
    <input type="hidden" value="{{ route('district.datatable') }}" id="link-datatable" />
@endsection
@section('script')
    @include('oc_core::layouts.partials.modal_delete_record')
    <script>
        jQuery(function($){
            var linkDataTable = $('#link-datatable').val();
            $('.table-be').dataTable({
                ajax: {
                    url: linkDataTable
                },
                order: [
                    [3, 'asc']
                ],
                dom: '<"row"<"col-md-7 offset-md-5"f>><"table-responsive"rt><"row"<"col-md-5"l><"col-md-7"p>><"clear">',
                columns: [
                    datatableRenderColumnNoOrder(),
                    {name: 'district_translations.name', data: 'name'},
                    {name: 'city_full_address', data: 'city_full_address', searchable: false, orderable: false},
                    {name: 'display_order', data: 'display_order', className: 'text-center'},
                    {name: 'active_text', data: 'active_text', className: 'text-center', searchable: false, orderable: false},
                    {name: 'action', data: 'action', className: 'text-center has-btn', searchable: false, orderable: false},
                ]
            }).on( 'draw.dt', function () {
                $(this).dataTable_init_js();
            });

            $('.table-be').on('click', '.btn-delete', function(e) {
                e.preventDefault();
                e.stopPropagation();
                var ele = e.currentTarget;
                var link = $(ele).attr('href');
                var msg = $(ele).attr('data-msg');
                if (!link || !msg) {
                    return;
                }
                $('#modal-delete-record .content').html(msg);
                $('#modal-delete-record form').prop({
                    action: link
                });
                $('#modal-delete-record').modal('show');
            });

        });
    </script>
@endsection
