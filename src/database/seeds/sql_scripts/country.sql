
INSERT INTO `countries` (`id`, `code`, `display_order`) VALUES (1, 'vn', 0);


-- ----------------------------
-- Records of country_translation
-- ----------------------------
INSERT INTO `country_translations` (`id`, `country_id`, `locale`, `name`) VALUES
  (1, 1, 'en', 'Vietnam'),
  (2, 1, 'vi', 'Việt Nam')
;

